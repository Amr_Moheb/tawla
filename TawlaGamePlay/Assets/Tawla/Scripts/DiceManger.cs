﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DiceManger : MonoBehaviour
{
      public Sprite[] diceSides;
      public Image FirstDiceImage;
       public Image SecDiceImage;
  public Color ShadowColor; 
 public Color LightColor; 
 public Color DefaultColor; 
 List<int> DicesValues = new List<int>();

    // Start is called before the first frame update


   
      
      private void Start ()
      {
         // rend = GetComponent<Image>();
         // StartCoroutine("RollTheDice");
         // diceSides = Resources.LoadAll<Sprite>("DiceSides/");
      }
      public void ThrowDices()
      {
            StartCoroutine("RollTheDice");
      }
    
      private void OnMouseDown()
      {
       //   StartCoroutine("RollTheDice");
      }
  
      // Coroutine that rolls the dice
      private IEnumerator RollTheDice()
      {
         DicesValues.Clear();
          int FirstDiceRandomSide = 0;
          int FirstDiceFinalSide = 0;
            int SecDiceRandomSide = 0;
          int SecDiceFinalSide = 0;
         float Timer = 0.05f;
          for (int i = 0; i <= 10; i++)
          {
               Timer +=0.02f;
              FirstDiceRandomSide = Random.Range(0, 5);
              SecDiceRandomSide = Random.Range(0, 5);
              if(i%2 == 0)
              {
                 FirstDiceImage.color = LightColor;
                  SecDiceImage.color = ShadowColor;
              }
              else
              {
                FirstDiceImage.color = ShadowColor;
                 SecDiceImage.color = LightColor;
              }
              
              FirstDiceImage.sprite = diceSides[FirstDiceRandomSide];
              SecDiceImage.sprite = diceSides[SecDiceRandomSide];
        //      Debug.Log("dd");
              yield return new WaitForSeconds(Timer);
          }
           FirstDiceImage.color = DefaultColor;
                 SecDiceImage.color = DefaultColor;
          FirstDiceFinalSide = FirstDiceRandomSide + 1;
          SecDiceFinalSide = SecDiceRandomSide + 1;
          FillDicesList(FirstDiceFinalSide,SecDiceFinalSide);

//          Debug.Log(FirstDiceFinalSide);
          GameManger.Instance.DicesThrowed(DicesValues);

         // DiceComplete(FirstDiceFinalSide);
          yield return null;
 
      }

      void FillDicesList(int firstDice, int SecondDice)
      {
         if(firstDice == SecondDice)
         {
            for (int i = 0; i < 4; i++)
            {
                DicesValues.Add(firstDice);
            }
         }
         else
         {
 DicesValues.Add(firstDice);
  DicesValues.Add(SecondDice);
         }

      }

      private void DiceComplete(int dieRoll)
      {
          Debug.Log("Complete");

         //Do whatever you want with the dieRoll here
      }
}
