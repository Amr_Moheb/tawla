﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TawlaUIManger : MonoBehaviour
{

    #region Singleton
     public static TawlaUIManger Instance
     {
         get
         {
             if (instance == null)
                 instance = FindObjectOfType(typeof(TawlaUIManger)) as TawlaUIManger;
 
             return instance;
         }
         set
         {
             instance = value;
         }
     }
    
     private static TawlaUIManger instance;
     #endregion
    public Button ThrowDiceBtn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ThrowDices()
    {
        GameManger.Instance.ThrowDices();
        DisableThrowDiceBtn();

    }
    public void EnableThrowDiceBtn()
    {
        ThrowDiceBtn.gameObject.SetActive(true);
    }
     public void DisableThrowDiceBtn()
    {
        ThrowDiceBtn.gameObject.SetActive(false);
    }
}
