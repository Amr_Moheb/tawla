﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
 
public class FadeAnimation : MonoBehaviour {
 
    // the image you want to fade, assign in inspector
    public Image img;
   
   private void Start() {
       StartAnimation();
   }
    
 
    IEnumerator FadeImage(bool fadeAway)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1f ; i >= 0.3; i -= Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(img.color.r, img.color.g, img.color.b, i);
                yield return null;
            }

        }
        
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0.3f ; i <= 1; i += Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(img.color.r, img.color.g, img.color.b, i);
                yield return null;
            }
        }
         StartCoroutine(FadeImage(!fadeAway));
    }

      public void StopAnimation()
    {
        // fades the image out when you click
        StopAllCoroutines();
       img.color = new Color(img.color.r, img.color.g, img.color.b, 1);
    }
    public void StartAnimation()
    {
//         print("StartAnimation");
         StartCoroutine(FadeImage(true));
    }
}
 