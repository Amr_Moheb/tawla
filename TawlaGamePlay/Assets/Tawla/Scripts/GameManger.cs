﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum GameMode
{
    Initial,
    Normal,
    Eating
}
public class GameManger : MonoBehaviour
{

 #region Singleton
     public static GameManger Instance
     {
         get
         {
             if (instance == null)
                 instance = FindObjectOfType(typeof(GameManger)) as GameManger;
 
             return instance;
         }
         set
         {
             instance = value;
         }
     }
    public GameMode CurrentGameMode ;
     private static GameManger instance;
     #endregion
      public DiceManger diceMangerInstance;
     List<int> DicesValues = new List<int>();
     public CheckerColor PlayerCheckerColor; 
     public List<TileManger> Tiles  = new List<TileManger>();
     public CheckerManger[] Checkers;
    int firstDiceValue = 0;
   int secondDiceValue = 0;
  
  bool IsFirstMove = true;


    // Start is called before the first frame update
    void Start()
    {
       

        // if the singleton hasn't been initialized yet
        
       //  DontDestroyOnLoad( this.gameObject );
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   public   void OneMovePlayed(TileManger fromTile, TileManger toTile)
    {
       

if(Tiles.IndexOf(toTile) >= 18 && IsMineTile(Tiles.IndexOf(toTile) ) )
{
    if(CurrentGameMode == GameMode.Initial)
    {
       CurrentGameMode = GameMode.Normal;
    }
    if(CurrentGameMode == GameMode.Normal)
    {
        print("CheckOnEatingMode");
            CheckOnEatingMode();
    }
    
}
        int PlayedDiceValue = Tiles.IndexOf(toTile) - Tiles.IndexOf(fromTile);
        DicesValues.Remove(PlayedDiceValue);
        if(DicesValues.Count == 0)
        {
         Reset();
         TawlaUIManger.Instance.EnableThrowDiceBtn();
          if(!IsFirstMove)
        {
             print("turn compleated");
        }
      

        }
        else
        {
             Reset();

         SetReadyToPlayTiles ();


        }
       
       
//        print("PlayedDice="+PlayedDiceValue);

       
    }

 void SetReadyToPlayTiles ()
 {
switch (CurrentGameMode)
{
    case GameMode.Initial:
    SetReadyToPlayTilesForInitialMode();
break;

case GameMode.Normal:
    SetReadyToPlayTilesForNormalMode();
break;


    default:
    break;
}
 }


    public void ThrowDices()
    {
       diceMangerInstance.ThrowDices();
    }
    public  TileManger GetTileByNumber(int num)
    {
          return Tiles[num];
    }
    public void DicesThrowed (List<int> DicesValues)
    {
        this.DicesValues = DicesValues;
//this.firstDiceValue = firstDiceValue;
//this.secondDiceValue = secondDiceValue;
SetReadyToPlayTiles();
    }
    void SetReadyToPlayTilesForNormalMode()
    {

        /*for (int i = 0; i < Tiles.Count; i++)
        {
         if(!IsTileAbleToPlay(i))
         {
           continue;
         }
         print("ValidTile="+i);
         GetLegalMoves(i);
        }*/
       CalculateReadyToPlayTilesFromPos(0);
    }
    void SetReadyToPlayTilesForInitialMode()
    {
        bool isLegalMovesExist = false;
        int StartPos = 1;
        if(IsFirstMove)
        {
         StartPos = 0;
         IsFirstMove = false;
        }
        CalculateReadyToPlayTilesFromPos(StartPos);
      /* for (int i = StartPos ; i < Tiles.Count; i++)
        {
         if(!IsTileAbleToPlay(i))
         {
           continue;
         }
         print("ValidTile="+i);

         if(GetLegalMoves(i))
         {
             isLegalMovesExist = true;
         }
        }
        if(!isLegalMovesExist)
        {
             SkipTurn();
        }*/


    }
    void CalculateReadyToPlayTilesFromPos(int startPos)
    {
         bool isLegalMovesExist = false;
        for (int i = startPos ; i < Tiles.Count; i++)
        {
         if(!IsTileAbleToPlay(i))
         {
           continue;
         }
     //    print("ValidTile="+i);

         if(GetLegalMoves(i))
         {
             isLegalMovesExist = true;
         }
        }
        if(!isLegalMovesExist)
        {
             SkipTurn();
        }
    }
    void CheckOnEatingMode()
    {
        for (int i = 0; i < 18; i++)
        {
         if(  IsMineTile(i))
         {
             return;
         }

        }
        SwitchToEatingMode();

    }
    void SwitchToEatingMode()
    {
        print("StartEatingMode");
        CurrentGameMode = GameMode.Eating;
    }
    void SkipTurn()
    {
        DicesValues.Clear();
        Reset();
         TawlaUIManger.Instance.EnableThrowDiceBtn();
    }

    bool GetLegalMoves(int initialTilePos)
    {
        bool hasLegalMove = false; 


        for (int i = 0; i < DicesValues.Count; i++)
        {
            if(IsTileAbleRecive(initialTilePos+DicesValues[i]))
        {
          Tiles[initialTilePos].AddLegalMoves(initialTilePos+DicesValues[i]);
          hasLegalMove = true;

        }
        }
        return hasLegalMove;
        
      /*   if(IsTileAbleRecive(initialTilePos+secondDiceValue))
        {
               print("secondDiceValue"+secondDiceValue);
                print("initialTilePos"+initialTilePos);
          Tiles[initialTilePos].AddLegalMoves(initialTilePos+secondDiceValue);
        }
        */

    }

    bool IsTileAbleRecive(int tileNumber)
    { 
        if(!IsValidTile(tileNumber))
        {
            return false;
        }
        return IsMineTile(tileNumber);

       
    }
    bool IsValidTile (int tileNumber)
    {
        if(tileNumber>23)
        {
            return false;
        }
        return true;
    }
    bool IsMineTile(int tileNumber)
    {
         if(Tiles[tileNumber].GetTileFillState() == TileFillState.HasBlackChecker && PlayerCheckerColor == CheckerColor.White)
            {
                  return false;
            }
              if(Tiles[tileNumber].GetTileFillState() == TileFillState.HasWhiteChecker && PlayerCheckerColor == CheckerColor.Black)
            {
                  return false;
            }
             return true;
    }
    bool IsTileAbleToPlay(int tileNumber)
    { 

//print(tileNumber);
             if(Tiles[tileNumber].GetTileFillState() == TileFillState.Empety)
            {
                 return false;
            }
        return IsMineTile(tileNumber);
       
    }

    public void Reset()
    {
        for (int i = 0; i < Tiles.Count; i++)
        {
            Tiles[i].SetTileBehaviour(TileState.None);
        }
        for (int i = 0; i < Checkers.Length; i++)
        {

             Checkers[i].SetCheckerState(CheckerState.None);
            Checkers[i].ClearLegalMoves();
        }

    }

    
}
