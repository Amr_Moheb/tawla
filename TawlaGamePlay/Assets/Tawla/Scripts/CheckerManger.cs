﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum CheckerState
{
    ReadyToPlay,
    Playing,
    
    OpenToRecive,
    None
}
public enum CheckerColor
{
    White,
    Black
    
}
public class CheckerManger : MonoBehaviour
{
  public int currentTileNumber  = 0;
  public  CheckerState CurrentState = CheckerState.None; 
  public CheckerColor checkerColor; 
public GameObject ReadyToPlayHighLight;
public GameObject OpenToReciveHighLight;
public Dragable DragingManger;
 TileManger TouchedTile; 
 TileManger CurrentTile;
 public List<TileManger> LegalMoves = new List<TileManger>();

    // Start is called before the first frame update
    void Start()
    {
        SetCheckerState(CurrentState);
//        print(GameManger.instance.name);
        AddCardToTile(GameManger.Instance.GetTileByNumber(currentTileNumber));
      //   firstLegalTile = GameManger.Instance.GetTileByNumber(Random.Range(1,23));
       //  SecondLegalTile = GameManger.Instance.GetTileByNumber(Random.Range(1,23));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetCheckerState(CheckerState cardState)
    {
        CurrentState = cardState;
        switch (cardState)
        {
       case CheckerState.ReadyToPlay:
       ActivateReadyToPlayState();
       break;
       case CheckerState.Playing:
       ActivatePlayingState();
       break;
       case CheckerState.OpenToRecive:
        ActivateOpenToReciveState();
        break;
        case CheckerState.None:
        ResetCardState();
        break;
        default:
        break;
        }
    }

    void ActivateReadyToPlayState()
    {//print("ActivateReadyToPlayState");
        ReadyToPlayHighLight.SetActive(true);
        ReadyToPlayHighLight.GetComponent<FadeAnimation>().StartAnimation();
        OpenToReciveHighLight.SetActive(false);
        DragingManger.EnableDraging();

    }
     void ActivatePlayingState()
    {
        ReadyToPlayHighLight.SetActive(true);
        ReadyToPlayHighLight.GetComponent<FadeAnimation>().StopAnimation();
    }
    
     void ActivateOpenToReciveState()
    {
        OpenToReciveHighLight.SetActive(true);
        ReadyToPlayHighLight.SetActive(false);
    }
    void ResetCardState()
    {
        ReadyToPlayHighLight.SetActive(false);
         OpenToReciveHighLight.SetActive(false);
         DragingManger.DisableDraging();
    }

    void OnTriggerEnter2D(Collider2D other) 
{
    if(other.tag == "tile")
    {
        if(other.GetComponent<TileManger>().tileBehaviourState == TileState.OpenToRecive)
        {
           TouchedTile = other.GetComponent<TileManger>(); 
            Debug.Log ("Triggered");
        }
       
    }
  
}

void OnTriggerExit2D(Collider2D other)
{
    if(other.tag == "tile")
    {
        TouchedTile = null;
         Debug.Log ("Exit Triggered");
    }
   
}


public void EndDragEvent ()
{
    if(TouchedTile != null)
      {
        AddCardToTile(TouchedTile);
      }else
      {
         AddCardToTile(CurrentTile);  
      }
      ReleaseLegalTiles();
}
public void BeginDragEvent()
{
  RemoveCardFromTile();  
  HighlightLegalTiles();

}
public void AddCardToTile(TileManger targetile)
{
     targetile.AddCard(this.gameObject);
    if(CurrentTile != targetile)
    {
        GameManger.Instance.OneMovePlayed(CurrentTile,targetile);
    }
    CurrentTile = targetile;
   
    

}
public void RemoveCardFromTile()
{
CurrentTile.RemoveCard();
}
public void AddLegalMoves(int LegalTileNumber)
{
LegalMoves.Add(GameManger.Instance.GetTileByNumber(LegalTileNumber));
}
public void ClearLegalMoves()
{
    LegalMoves.Clear();
}
public void HighlightLegalTiles()
{
for (int i = 0; i < LegalMoves.Count; i++)
{
    LegalMoves[i].SetTileBehaviour(TileState.OpenToRecive);
}


}
public void ReleaseLegalTiles()
{
for (int i = 0; i < LegalMoves.Count; i++)
{
    LegalMoves[i].SetTileBehaviour(TileState.None);
}

}
public CheckerColor GetCheckerColor ()
{
return checkerColor;
}
}
