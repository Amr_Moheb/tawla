﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum TileFillState
{
    HasWhiteChecker,
    HasBlackChecker,
    Empety
    
}
public enum TileState
{
    OpenToRecive,
    ReadyToPlay,
    None
}
public class TileManger : MonoBehaviour
{
    public List<CheckerManger> Checkers;
    public TileState tileBehaviourState;
    public TileFillState tileFillState;
    public Image TileImage;
    public Color HighLightColor;
    public Color DeHighLightColor;
    public RectTransform CheckerContainer;
    public int CountOfCheckers = 0;
    public RectTransform[] CheckersPositions; 
    public Text MoreCheckersCount; 
  public GameObject MoreCheckersLabel; 
    // Start is called before the first frame update
    void Start()
    {
      ApplyStateBehaviour();
    }

    // Update is called once per frame
    void Update()
    {
     //   ApplyStateBehaviour(); 
    }
    public void AddCard(GameObject Checker)
    {
Checkers.Add(Checker.GetComponent<CheckerManger>());

CountOfCheckers ++;
if(CountOfCheckers < 5)
{
Checker.GetComponent<RectTransform>().parent = null;
Checker.GetComponent<RectTransform>().parent = CheckerContainer;
Checker.GetComponent<RectTransform>().position = CheckersPositions[CountOfCheckers-1].position;
}
else
{
Checker.GetComponent<RectTransform>().parent = null;
Checker.GetComponent<RectTransform>().parent = CheckerContainer;
Checker.GetComponent<RectTransform>().position = CheckersPositions[4].position;
}


Checker.GetComponent<CheckerManger>().SetCheckerState(CheckerState.None);
UpdateMoreCheckerLabel();
  ApplyStateBehaviour();
  UpdateTileFillState();

    }


    public void RemoveCard()
{
Checkers.RemoveAt(Checkers.Count-1);
CountOfCheckers --;
UpdateMoreCheckerLabel();
ApplyStateBehaviour();
UpdateTileFillState ();

}
void UpdateTileFillState ()
{
   if(Checkers.Count == 0)
   {
      tileFillState = TileFillState.Empety;
     
   }else if (Checkers[Checkers.Count-1].GetCheckerColor() == CheckerColor.Black)
   {
       tileFillState = TileFillState.HasBlackChecker;
   }else if (Checkers[Checkers.Count-1].GetCheckerColor() == CheckerColor.White)
   {
       tileFillState = TileFillState.HasWhiteChecker;
   }

}
 void UpdateMoreCheckerLabel()
 {
     if(CountOfCheckers > 5)
     {
         MoreCheckersLabel.SetActive(true);
         MoreCheckersCount.text = "" + (CountOfCheckers);
         

     }else
     {
        MoreCheckersLabel.SetActive(false);
         
     }
 }

void ApplyStateBehaviour()
{

    switch (tileBehaviourState)
    {
        case TileState.OpenToRecive:
           SetHighLight(true);
           break;
           case TileState.ReadyToPlay:
           SetHighLight(false);
           ReadyToPlayBehaviour();
           break;
           case TileState.None:
           SetHighLight(false);
           break;
        default:
        break;
    }

}

public void ReadyToPlayBehaviour()
{
    this.GetComponent<BoxCollider2D>().enabled = false;
    if(Checkers.Count == 0)
    {
        return;
    }
for (int i = 0; i < Checkers.Count; i++)
{
Checkers[i].SetCheckerState(CheckerState.None);
}

Checkers[Checkers.Count-1].SetCheckerState(CheckerState.ReadyToPlay);



}

 public void SetHighLight(bool isHighLight)
 {
     if(isHighLight)
     {
         this.GetComponent<BoxCollider2D>().enabled = true;
TileImage.color = HighLightColor;
SetAllCheckersState(CheckerState.OpenToRecive);


     }else
     {
         this.GetComponent<BoxCollider2D>().enabled = false;
TileImage.color = DeHighLightColor;
SetAllCheckersState(CheckerState.None);
     }

 }
 void SetAllCheckersState(CheckerState state )
 {
for (int i = 0; i < Checkers.Count; i++)
{
    Checkers[i].SetCheckerState(state) ;
}

 }
 public void SetTileBehaviour(TileState state)
 {
tileBehaviourState = state;
ApplyStateBehaviour();
 }
 public TileFillState GetTileFillState()
 {
     return tileFillState;
 }

 public void AddLegalMoves(int legalTileNumber)
 {
tileBehaviourState = TileState.ReadyToPlay;
ApplyStateBehaviour();
Checkers[Checkers.Count-1].AddLegalMoves(legalTileNumber);
 }
}
